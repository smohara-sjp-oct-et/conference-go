import json

import requests

from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_weather_info(city, state):
    pass
    # geocode params
    # q = city, state and country code
    # appid = api key
    # limit = num locations, opt

    geocode_params = {
        "q": f"{city},{state},US",
        "limit": 1,  # only need 1
        "appid": OPEN_WEATHER_API_KEY,  # should be last??
    }

    # requests
    url = "http://api.openweathermap.org/geo/1.0/direct"
    geocode_response = requests.get(url, params=geocode_params)

    # find lat and lon in response
    geocode_info = json.loads(geocode_response.content)

    try:
        latitude = geocode_info[0]["lat"]
        longitude = geocode_info[0]["lon"]
    except (IndexError, KeyError):
        return None  # catch potential errors

    # get weather params
    # lat = req
    # lon = req
    # appid = api key req
    # units = opt, def=standard, metric, imperial

    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",  # don't want Kelvin
        "appid": OPEN_WEATHER_API_KEY,  # should be last??
    }

    # requests
    url = "https://api.openweathermap.org/data/2.5/weather"
    weather_response = requests.get(url, params=weather_params)

    weather_info = json.loads(weather_response.content)

    # return data
    try:
        return {
            "temperature": weather_info["main"]["temp"],
            "description": weather_info["weather"][0]["description"],
        }
    except (IndexError, KeyError):
        return None  # again, catch errors


def get_location_pic(city, state):
    pass
    # get photo for location

    # Pexels requests in the guidelines to show a prominent link to Pexels
    # and to credit the photographer link can be done easy via front end
    # as not a true public facing entity will currently omit credit as not
    # currently part of the class material

    # Pexels requires auth via header
    pexel_auth_header = {"Authorization": PEXELS_API_KEY}

    # search params
    # query = req, str, "city state"
    # orientation = opt, str, landscape, portrait, square
    # size = opt, str, small is 4mp, medium is 12mp, large 24mp
    # color = opt, str, support hex, and sever named
    # local = opt, str, locale to use for search query
    # page = opt, int, page requested
    # per_page = opt, int, def=15, max=80
    photo_search_params = {
        "query": f"{city}, {state}",
        "orientation": "square",  # personal preference
        "size": "medium",  # don't want to get too big or small
        "per_page": 1,  # only need 1
    }

    # requests
    url = "https://api.pexels.com/v1/search"
    photo_search_response = requests.get(
        url, params=photo_search_params, headers=pexel_auth_header
    )
    photo_search_info = json.loads(photo_search_response.content)

    # return url for the data
    try:
        return {"picture_url": photo_search_info["photos"][0]["src"]["medium"]}
    except (IndexError, KeyError):
        return {"picture_url": None}
